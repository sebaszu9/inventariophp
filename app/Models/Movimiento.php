<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
    use HasFactory;

    protected $table = 'articulos_movimiento';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'cedula_mov',
        'nombre_mov',
        'fecha_mov',
        'id_mov',
        'id_producto',
        'cantidad',
        'valor',
    ];

    public function tipo_producto(){
        //return $this->belongsTo("App\Models\Linea","id_linea");
        return $this->HasOne("App\Models\Producto","id","id_producto");
    }
}
