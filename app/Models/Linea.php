<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Linea extends Model
{
    protected $table = 'linea';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo',
        'descripcion',
        'deleted',
    ];

    public function sublineas()
    {
      return $this->hasMany('App\Models\Sublinea','codigo','categoria_id');
    }
}
