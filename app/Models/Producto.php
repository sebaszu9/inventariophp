<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'datos_producto';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'codigo_producto',
        'id_linea',
        'id_sublinea',
        'nom_producto',
        'descripcion',
        'costo_ultimo',
        'stock',
        'deleted',
    ];

    public function tipo_linea(){
        //return $this->belongsTo("App\Models\Linea","id_linea");
        return $this->HasOne("App\Models\Linea","codigo","id_linea");
    }

    public function tipo_sublinea(){
        //return $this->belongsTo("App\Models\Sublinea","id_sublinea");
        return $this->HasOne("App\Models\Sublinea","id_subcategoria","id_sublinea");
    }
}
