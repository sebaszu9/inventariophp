<?php

namespace App\Http\Controllers;

use Log;
use PDF;
use Mail;

use App\Models\Movimiento;
use App\Models\Producto;

use Illuminate\Http\Request;

class MovimientoController extends Controller
{
    //
    public function movimientos()
    {
      return view('admon.MovimientosAdmon');
    }

    public function guard_movimiento(Request $request){
      try {

        $valor = json_decode($request['lista']);
        $id = $request['id'];

        foreach ($valor as $value) {

          $idProducto = $value->producto->id;
          // operacion inventario
          $product = Producto::find($idProducto);

          $ahora = 0;
          $anterior = $product->stock;
          $cantidad = $value->cantidad;

          $valortotal = 0;

          if ($request['id_mov']==1) {
            $ahora = $anterior + $cantidad;
            $valortotal = $request['valor'];
          }
          else {
            $ahora = $anterior - $cantidad;
          }

          $data['id_mov'] = $request['id_mov'];
          $data['fecha_mov'] = $request['fecha_mov'];
          $data['cedula_mov'] = $request['cedula_mov'];
          $data['nombre_mov'] = $request['nombre_mov'];
          $data['valor'] = $request['valor'];
          $data['id_producto'] = $product->id;
          $data['cantidad'] = $value->cantidad;
          Movimiento::create($data);

          // actualizar stock
          if($request['id_mov']==1){
            $product->update([
              "stock" => $ahora,
              "costo_ultimo" => $valortotal
            ]);
          }else {
            $product->update([
              "stock" => $ahora,
            ]);
          }
        }
        return response()->json([ 'message' => "Successfully created", 'success' => true ], 200);

      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }
    }

    public function obtain_Movimiento(){
      $data = Movimiento::with("tipo_producto")->get();
      return $data;
    }


    

    public function download()
    {
        $movi = Movimiento::with("tipo_producto")->get();

        $data = [
            'movi' => $movi
        ];

        Log::info($data);
    

        $pdf = PDF::loadView('admon.ReportesAdmon', $data);
        return $pdf->setPaper('a4', 'landscape')->stream('archivo.pdf');
    }

}
