<?php

namespace App\Http\Controllers;

use Log;
use Auth;

use App\Models\Admon;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    //
    public function encrypted()
    {
        $pass = "123456";
        $encript = Hash::make($pass);
        echo $encript;
    }

    public function form_log_admon()
    {
        return view('admon.LoginAdmon');
    }

    public function login_admon(Request $request)
    {
        Log::info("prueba");
        Log::info($request);
        $user = Admon::first();
        $login=Auth::guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]);
        if ($login) {
        Log::info("hecho");
        Auth::guard('admin')->login($user);
        return response()->json('Authenticated',200);
        }else{
        return response()->json(['errors'=>'Email o contrasena incorrectos.'],422);
        }

    }
}
