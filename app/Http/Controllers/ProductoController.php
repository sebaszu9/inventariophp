<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Linea;
use App\Models\Sublinea;
use App\Models\Producto;
use App\Models\Movimiento;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    //
    public function productos()
    {
      return view('admon.ProductosAdmon');
    }

    public function inventario()
    {
      return view('admon.HomeAdmon');
    }

    public function obtain_linea(){
      $linea = Linea::get();
      return $linea;
    }

    public function obtain_sublinea(){
      $sublinea = Sublinea::get();
      return $sublinea;
    }

    public function guard_categoria (Request $request){
      try {
        Log::info($request);
        $codigo = $request['codigo'];

        $data['descripcion'] = $request['descripcion'];

        Linea::create($data);

        return response()->json([ 'message' => "Successfully created", 'success' => true ], 200); 
      
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }
    }

    public function guard_sub (Request $request){
      try {
        Log::info($request);
        $id_subcategoria = $request['id_subcategoria'];

        $data['id_categoria'] = $request['id_categoria'];
        $data['descripcion'] = $request['descripcion'];

        Sublinea::create($data);

        return response()->json([ 'message' => "Successfully created", 'success' => true ], 200); 
      
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }
    }

    public function guard_producto(Request $request){

      try {

        Log::info($request);
        $id = $request['id'];

        $data['codigo_producto'] = $request['codigo_producto'];
        $data['id_linea'] = $request['id_linea'];
        $data['id_sublinea'] = $request['id_sublinea'];
        $data['nom_producto'] = $request['nom_producto'];
        $data['descripcion'] = $request['descripcion'];
        
        
        if($id > 0){
          Producto::find($id)->update($data);

        }else{
          $data['costo_ultimo'] = 0;
          $data['stock'] = 0;
          Producto::create($data);
        }
        return response()->json([ 'message' => "Successfully created", 'success' => true ], 200); 
      
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }

    }

    public function obtain_Product(){
      $data = Producto::with("tipo_linea", "tipo_sublinea")->where('deleted',0)->get();
      return $data;
    }

    public function deleted_product(Request $request){
      Producto::where('id', $request['id'])->update([
        'deleted'=>1
      ]);
  }
}
