<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Inventario PHP</title>
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
  rel="stylesheet">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;1,300&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>

<body style = "overflow-x: hidden;">

  <!-- React root DOM -->
  <div id="HomePage" class = "full-page">
  </div>

  <!-- React JS -->
  <script src="{{ mix('js/app.js') }}" defer></script>

</body>
<style>

body {
    background-color: white;
    font-family: "Open Sans", sans-serif;
}


.p-footer{
  color: white;
  border-right: 1px solid white;
}
.p-footer-2{
  color: white;
}
.p-footer-3{
  color: #e0ad24;
}
.footer{
  position: absolute;
  bottom: 0;
  right: 0;
  width: 100%;
  height: 90px;
  box-shadow: -16px -102px 218px 67px rgb(0 0 0 / 64%);
  border-top: 2px solid white;
}

</style>
</html>