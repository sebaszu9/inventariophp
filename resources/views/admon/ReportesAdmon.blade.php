<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        @page {
            margin: 0cm 0cm;
            font-size: 1em;
        }
        body {
            margin: 3.5cm 1cm 1cm;
        }
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 3cm;
            background-color: #46C66B;
            color: white;
            text-align: center;
            line-height: 30px;
        }
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 1cm;
            background-color: #46C66B;
            color: white;
            text-align: center;
            line-height: 35px;
        }
    </style>
</head>
<body>
    <header>
        <br>
        <h1><strong>Sistema de inventario</strong></h1>
    </header>
    <main>
            <h5 style="text-align: center"><strong>Reporte general</strong></h5>
            <table class="table table-striped text-center">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Cedula</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Fecha del movimiento</th>
                        <th scope="col">Nombre del producto</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Acción</th>
                    </tr>
                </thead>
               <tbody>
                @foreach ($movi as $data)
                <tr>
                  <th scope="row">{{$data->id}}</th>
                  <td>{{$data->cedula_mov}}</td>
                  <td>{{$data->nombre_mov}}</td>
                  <td>{{$data->fecha_mov}}</td>
                  <td>{{$data->tipo_producto->nom_producto}}</td>
                  <td>{{$data->cantidad}}</td>
                  <td>{{$data->valor}}</td>
                  @if ($data->id_mov === 1)
                      <td style="color: #00ad5f">Entrada</td>
                    @else
                      <td style="color: red">Salida</td>
                  @endif
                </tr>
                @endforeach
                </tbody>
            </table>
    </main>
    <footer>
        <h3><strong>Sebastian - Chamo - Choujy - Danny </strong></h3>
    </footer>
</body>

</html>