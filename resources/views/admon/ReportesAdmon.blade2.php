<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF</title>

</head>
<style>
  @page {
      margin: 0cm 0cm;
      font-family: Arial;
  }

  body {
      margin: 3cm 2cm 2cm;
  }

  header {
      position: fixed;
      top: 0cm;
      left: 0cm;
      right: 0cm;
      height: 2cm;
      background-color: rgba(0, 0, 0, 0.125);;
      color: white;
      text-align: center;
      line-height: 30px;
  }

  footer {
      position: fixed;
      bottom: 0cm;
      left: 0cm;
      right: 0cm;
      height: 2cm;
      background-color: rgba(0, 0, 0, 0.125);;
      color: white;
      text-align: center;
      line-height: 35px;
  }

  .img{
  width: 400px;
  height: 100px;
  }
  

  table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  }

</style>

<body>
  <header>
    <h1>Reporte general</h1>
  </header>
        
  <div class="container">
    <div class="">

    </div>
      <div class="col-md-12">
        <table class="table table-inverse">
          <thead style="background-color: rgba(0, 0, 0, 0.125);">
            <tr >
              <th scope="col">Id</th>
              <th scope="col">Cedula </th>
              <th scope="col" style="width: 150px">Nombre </th>
              <th scope="col">Fecha del movimiento </th>
              <th scope="col">Acción </th>
              <th scope="col">Nombre del producto </th>
              <th scope="col">Cantidad </th>
              <th scope="col">Valor </th>
            </tr>
          </thead>
          <tbody>
                  @foreach ($movi as $data)
                    <tr scope="row">
                      <td>{{$data->id}}</td>
                      <td>{{$data->cedula_mov}}</td>
                      <td>{{$data->nombre_mov}}</td>
                      <td>{{$data->fecha_mov}}</td>
                      @if ($data->id_mov === 1)
                          <td style="color: #00ad5f">Entrada</td>
                        @else
                          <td style="color: red">Salida</td>
                      @endif
                      <td>{{$data->tipo_producto->nom_producto}}</td>
                      <td>{{$data->cantidad}}</td>
                      <td>{{$data->valor}}</td>
                    </tr>
                  @endforeach

          </tbody>
        </table>

      </div>
</div>

</body>

</html>