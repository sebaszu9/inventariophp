import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import '../../../css/app.scss';
import Select from 'react-select';
import makeAnimated from 'react-select/animated'
import {ip} from '../ApiRest';
import Swal from 'sweetalert2';
import { Button, Modal } from 'react-bootstrap';
import useful from "../util";
import Pagination from '../pagination/Pagination';
import PaginationButton from '../pagination/Pagination-button';

function ModalRegister(props) {

    const [ loading, setLoading ] = useState(false)

    const [ animatedComponents, setAnimatedComponents ] = useState(makeAnimated)
    const [ currentPage, setCurrentPage ] = useState(1)
    const [ postsPerPage, setPostsPerPage ] = useState(6)
    const [ id, setId ] = useState(null);
    const [ data, setData ] = useState({});
    const [ show, setShow ] = useState(false);
    const [ cedula, setCedula ] = useState(null);
    const [ nombrePer, setNombrePer ] = useState(null);
    const [ fecha, setFecha ] = useState(null);
    const [ listaProduct, setListaProduct ] = useState([])
    const [ producto, setProducto ] = useState(null);
    const [ productoSelect, setProductoSelect ] = useState([]);
    const [ cantidad, setCantidad ] = useState(null);
    const [ compraTotal, setCompraTotal ] = useState([]);
    const [ text, setText ] = useState(null);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const modal = async () => {
        setShow(true);
    }

    const [ listMovimiento, setListMovimiento ] = useState([]);
    const [ tipoMovim, setTipoMovim ] = useState(0);
    const [ tipoMovimList, setTipMovimList ] = useState([
        {
        value:'1',
        label:'Entrada',
        },
        {
        value:'2',
        label:'Salida',
        },

    ]);

    useEffect(()=>{
      obtain_Product();

    }, []);

    const MessageError = async (data) => {
      Swal.fire({
        title: 'Error',
        text: data,
        icon: 'warning',
      })
    }
  
    const MessageSuccess = async (data) => {
      Swal.fire({
        text: data,
        icon: 'success',
      })
    }

    const obtain_Product = () => {
      axios.get(ip+'obtain_Product').then(response=>{
        var res = response.data;
        setListaProduct(response.data)
        var producto = []
        res.map(item=>{
          var data = {
            value:item.id,
            label:item.nom_producto
          }
          producto.push(data)
        })
        setProductoSelect(producto);
      })
    }

    const save_movimiento = async() => {
      var message = ''
      var error = false
      if (tipoMovim == null){
        error = true
        message = "Escoge tipo de movimiento "
      }else if(fecha == null){
        error = true
        message = "Elige la fecha "
      }else if(cedula == null){
        error = true
        message = "Escribe la cédula de la persona "
      }else if(nombrePer == null){
        error = true
        message = "Escribe el nombre de la persona "
      }
      if(error){
          MessageError(message)
      }
      else{
        const data = new FormData()
        data.append('id',props.id)
        data.append('id_mov', tipoMovim.value)
        data.append('fecha_mov', fecha)
        data.append('cedula_mov', cedula)
        data.append('nombre_mov', nombrePer)
        data.append('valor', compraTotal)
        data.append('lista', JSON.stringify(listMovimiento))
        axios.post(ip+'guard_movimiento',data).then(response=>{
            if(props.id == null){
              MessageSuccess("Movimiento creado correctamente");
              props.handleClose();
              
            }else{
              MessageSuccess("Producto editado correctamente");
              props.handleClose();
          }
        })
      }
    }

    const onClickAdd  = async () =>{
      setProducto(0)
      setCantidad('')

      console.log("entro onclick add")
      console.log(tipoMovim);

      if(tipoMovim.value > 0){
        const idProducto = producto.value
        const i = listaProduct.findIndex((item) => item.id == idProducto);
        console.log(listaProduct[i]);
        const carrito = listMovimiento
        const producto2 = listaProduct[i]

        let anterior = parseInt(producto2.stock)
        let ahora = tipoMovim.value == 1 ? anterior + parseInt(cantidad) : anterior - parseInt(cantidad)
        const item = {
          producto: producto2,
          anterior: anterior,
          cantidad: cantidad,
          ahora: ahora
        }
         carrito.push(item)
         setListMovimiento(carrito)
         setText(producto2.id)

         /*const data = compraTotal
         var  valor = 0
         const total2 = {
           total2: valor + listaProduct[i].costo_ultimo
         }
         data.push(total2)
         console.log(data);
        setCompraTotal(data)*/
         

      }
      else {
        MessageError("Debe seleccionar tipo de movimiento");
      }

    }
    const listDeleted = (item) => {
      const limpia = listMovimiento.filter((_item, index) => index !== item);
      setListMovimiento(limpia)
    }
      
    console.log(tipoMovim);
    return(
        <div>
     
              <div className="container-fluid">
                <div className="row">
                <div className="col col-md-6 mb-3">
                  <label for="recipient-name" class="col-form-label">Tipo de movimiento:</label>
                  <Select
                  value={tipoMovim}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={tipoMovimList}
                  onChange={(e)=>setTipoMovim(e)}
                  placeholder = "Seleccionar tipo de movimiento"
                  name="colors"
                  />
                </div>
                <div class="col-md-6 mb-3">
                  <label for="recipient-name" className="col-form-label">Fecha:</label>
                  <input type="date" className="form-control" placeholder="Ingrese nombre de la persona" value={fecha} onChange={(event)=>setFecha(event.target.value)}/>
                </div>
                <div className="col-md-12 mb-3">
                  <label for="recipient-name" className="col-form-label">Cédula de la persona:</label>
                  <input type="text" className="form-control" placeholder="Ingrese cédula de la persona" value={cedula} onChange={(event)=>setCedula(event.target.value)}/>
                </div>
                <div className="col-md-12 mb-3">
                  <label for="recipient-name" className="col-form-label">Nombre de la persona:</label>
                  <input type="text" className="form-control" placeholder="Ingrese nombre de la persona" value={nombrePer} onChange={(event)=>setNombrePer(event.target.value)}/>
                </div>
                <div className="row">

                <div class="col col-md-6">
                  <label for="recipient-name" class="col-form-label">Producto:</label>
                    <Select
                      value={producto}
                      closeMenuOnSelect={true}
                      components={animatedComponents}
                      options={productoSelect}
                      onChange={(e)=>setProducto(e)}
                      placeholder = "Seleccionar producto"
                      name="colors"
                    />
                </div>
                <div class="col col-md-4">
                  <label for="recipient-name" className="col-form-label">Cantidad:</label>
                  <input type="text" className="form-control" placeholder="Ingrese cantidad" value={cantidad} onChange={(event)=>setCantidad(event.target.value)}/>
                </div>
                <div class="col col-md-2">
                  <label for="recipient-name" className="col-form-label" style={{color: 'white'}}>ejemplo</label>
                  <button type="button" class="btn btn-primary" onClick={()=>onClickAdd()}>Agregar</button>
                </div>
                <div className="mt-4  table-responsive table--no-card m-b-30">
                    <table className="table table-hover">
                      <thead className="table-dark">
                        <tr>
                          <th>ID</th>
                          <th>Producto</th>
                          <th>Cantidad anterior</th>
                          <th>Cantidad ingresar</th>
                          <th>Cantidad ahora </th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          listMovimiento.map((item, i)=>{
                            return(
                              <tr>
                                <td>{item.producto.id}</td>
                                <td>{item.producto.nom_producto}</td>
                                <td>{item.anterior}</td>
                                <td>{item.cantidad}</td>
                                <td>{item.ahora}</td>
                                <td>
                                  <button className="btn" onClick={()=>listDeleted(i)}>
                                  <i class = "material-icons">delete</i>
                                  </button>
                                </td>
                              </tr>
                            )
                          })
                        }
                      </tbody>
                    </table>

                </div>


              </div>
                <div class="col-md-12 mb-3">
                  <label for="recipient-name" className="col-form-label">Valor total de la compra:</label>
                  <input type="text" className="form-control" placeholder="Ingrese valor total de la compra" value={compraTotal} onChange={(event)=>setCompraTotal(event.target.value)}/>
                </div>
                </div>
              </div>
              <Modal.Footer>
             <Button variant="primary" onClick = {()=>save_movimiento()}>Guardar movimiento</Button>
             <Button variant="danger" onClick = {()=>props.handleClose()}>Cerrar</Button>
            </Modal.Footer>
      </div>
    );
}

export default ModalRegister;

if (document.getElementById('ModalRegister')) {
  ReactDOM.render(<ModalRegister />, document.getElementById('ModalRegister'));
}