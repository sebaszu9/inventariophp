import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import '../../../css/app.scss';
import Select from 'react-select';
import makeAnimated from 'react-select/animated'
import {ip} from '../ApiRest';
import Swal from 'sweetalert2';
import { Button, Modal } from 'react-bootstrap';
import useful from "../util";
import Pagination from '../pagination/Pagination';
import PaginationButton from '../pagination/Pagination-button';

import Registro from './Modal';

function Movimientos(props) {

  const [loading, setLoading] = useState(false)

  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [id, setId] = useState(null);
  const [data, setData] = useState({});
  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);
  const [detalles, setDetalles] = useState([])
  const [listMovimiento, setListMovimiento ] = useState([]);
  const [backProduct, setBackProduct] = useState(listMovimiento);
  const [backProduct2, setBackProduct2] = useState(listMovimiento);
  const [producto, setProducto] = useState([])
  const [validar, setValidar] = useState(false)

  
  const handleClose = () =>{
    setShow(false);
    setShow2(false);
    setValidar(false)
    mostrarMovimiento();
  }

  const updateCurrentPage = async (number) => {
    await setListMovimiento([])
    await setCurrentPage(number)
    const dataNew = Pagination.paginate(backProduct,number,postsPerPage)
    await setListMovimiento(dataNew)
  }

  const handleShow = () => setShow(true);

  const modal = async () => {
    setShow(true);
  }
  const [ token, setToken ] = useState(null)

  useEffect(()=>{
    mostrarMovimiento();
    obtener_producto();
    var csrf = document.querySelector('meta[name="csrf-token"]').content;
    setToken(csrf);

    //validar();
}, []);

  const mostrarMovimiento = async() =>{
    axios.get(ip+'obtain_Movimiento').then(response=>{
      console.log(response);
      var res = response.data;
      setBackProduct(response.data)
      setBackProduct2(response.data)
      const dataNew = Pagination.paginate(response.data,currentPage,postsPerPage)
      setListMovimiento(dataNew)
    })
  }

  const dataDet = async (data) => {
    console.log("Aqui detalles")
    console.log(data)
  
    setShow2(true);
  
    setDetalles(data);
    
    var tipoproduct = data.id_producto
    obtener_producto(tipoproduct, true)
  
  }

  const obtener_producto = (data) =>{
    axios.get(ip+'obtain_Product').then(response=>{
        var res = response.data;
        var filtro = res.filter(e=>e.id == data )
        console.log("aqui la data producto");
        console.log(data);
        console.log(filtro);
  
        setProducto(filtro)
    })
  }

  return (
  <section className = "container-app">
    <div className="container mt-5"  >
        <div className="btn container col-1 "><a href ={ip+"inventario"}>
          <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4></a>
        </div>
        <div className="text-center">
        <h4><strong>Módulo de movimientos</strong></h4>
        </div>
        <div className="row mb-3">
        <div className="col-md-12 row mb-3 p-0 mt-4">
          <div className="row m-t-30">
            <div className="mt-4 mb-3">
              <form className="d-flex mt-3 mb-3" action={ip+"reportes"} method="GET" target="_blank">
              <input type="hidden" name="_token" value={token}/>
              
                <button onClick = {()=>modal()} type="button" class="btn btn-color"> <span className="material-icons icon-btn">add</span>Realizar movimiento</button>

                <button type="submit" class="btn btn-color" style={{marginLeft: '15px'}}> <span className="material-icons icon-btn"><img style={{width:25, height:25}} src={ip+'images/pdf.png '}/></span>Reporte general</button>

              </form>
            </div>
            <div className="col-md-12">
              <table className="table table-hover" style={{marginBottom: '40px', paddingRight: '1px'}}>
                <thead className="table-dark">
                  <tr>
                    <th>Id</th>
                    <th>Cédula</th>
                    <th>Nombre</th>
                    <th>Fecha del movimiento</th>
                    <th>Valor total</th>
                    <th>Accion</th>
                    <th>Detalles</th>
                  </tr>
                </thead>
                <tbody>
                {
                  listMovimiento.map((item)=>{
                    return(
                      <tr>
                        <th scope="row">{item.id}</th>
                        <td>{item.cedula_mov}</td>
                        <td>{item.nombre_mov}</td>
                        <td>{item.fecha_mov}</td>
                        <td>$ {useful.convertMoney(item.valor)}</td>
                        <td style={item.id_mov == 1 ? {color:'#00ad5f'} : {color: 'red'} }>{item.id_mov == 1 ? 'Entrada' : 'Salida'}</td>
                        <td><div className = "">
                          <button onClick = {()=>dataDet(item)} className="btn "><i class = "material-icons">visibility</i></button>
                        </div></td>
                      </tr>
                    )
                  })
                }
                </tbody>
              </table>
              <div className="d-flex col-md-12 col-12 justify-content-end">
                <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backProduct.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
              </div>
              </div>
            </div>
            <Modal show={show} onHide={handleClose} size="lg">
           <Modal.Header closeButton>
             <Modal.Title>{id > 0 ? 'Editar productos':'Realizar movimientos'}</Modal.Title>
           </Modal.Header>
             <Modal.Body>
               <Registro handleClose={()=>handleClose()} data={data} nom={validar} id = {id}/>
             </Modal.Body>
         </Modal>

        {/*////Modal detalles//////*/}
         <Modal show={show2} onHide={handleClose}>
           <Modal.Header closeButton>
             <Modal.Title>Detalles del movimiento</Modal.Title>
           </Modal.Header>
             <Modal.Body>
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-8">
                    <strong>Id del movimiento:</strong> {detalles.id_mov == 1 ? 'Entrada' : 'Salida'}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                  <strong>Cédula de la persona:</strong> {detalles.cedula_mov}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                  <strong>Nombre completo:</strong> {detalles.nombre_mov}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                  <strong>Fecha del movimiento:</strong> {detalles.fecha_mov}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                  <strong>Producto:</strong> 
                  {
                    producto.map(item=>{
                    return(
                      <div>
                        <p>{item.nom_producto}</p>
                      </div>
                    )
                  })}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                  <strong>Cantidad:</strong> {detalles.cantidad}
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                  <strong>Valor:</strong> {detalles.valor}
                  </div>
                </div>
              </div>
             </Modal.Body>
         </Modal>
          </div>
        </div>
    </div>
  </section>
    );
  }
  export default Movimientos;

  if (document.getElementById('Movimientos')) {
    ReactDOM.render(<Movimientos />, document.getElementById('Movimientos'));
  }