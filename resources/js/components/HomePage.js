import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import '../../css/app.scss'
import {ip} from './ApiRest';
import Swal from 'sweetalert2'
function HomePage() {

  const [loading, setLoading] = useState(false)


  return (
  <section className = "container-app">
    <div className="container mt-5"  >
          <div className="text-center">
          <h4><strong>Módulo Inventario</strong></h4>
          </div>
        <div className="row mb-3" style={{marginLeft:'100px', justifyContent:'center'}}>
        <div className="col-md-9 row mb-3 p-0 mt-4" style={{marginLeft: '110px'}}>

        <div className="col-md-4 mb-3">
        <a href={ip+"productos"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/productos.png'}/>
                </div>
                <div className="mt-2">
                  <p className="m-0">Productos</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>

        <div className="col-md-4 mb-3">
        <a href={ip+"movimientos"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/salidas.png '}/>
                </div>
                <div className="mt-2">
                  <p className="m-0">Movimientos</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>

        {/*<div className="col-md-4 mb-3 ">
          <a href={ip+"admon/conjunto/propietarios"}>
          <div className="card quitar-borde cursor-pointer" style={{position: 'relative'}}>
            <div className="card-body hover-card">
              <div className="text-center">
                <div>
                  <img style={{width:50, height:50}} src={ip+'images/salidas.png'}/>
                </div>
                <div className="mt-2">
                  <p className="m-0">Salidas</p>
                </div>
              </div>
            </div>
          </div>
        </a>
        </div>//*/}
        </div>
        </div>
    </div>
  </section>
    );
  }
  export default HomePage;

  if (document.getElementById('HomePage')) {
    ReactDOM.render(<HomePage />, document.getElementById('HomePage'));
  }