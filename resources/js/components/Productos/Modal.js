import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import '../../../css/app.scss';
import Select from 'react-select';
import makeAnimated from 'react-select/animated'
import {ip} from '../ApiRest';
import Swal from 'sweetalert2';
import { Button, Modal } from 'react-bootstrap';
import useful from "../util";
import Pagination from '../pagination/Pagination';
import PaginationButton from '../pagination/Pagination-button';

function ModalRegister(props) {

    const [loading, setLoading] = useState(false)

    const [ animatedComponents, setAnimatedComponents ] = useState(makeAnimated)
    const [ currentPage, setCurrentPage ] = useState(1)
    const [ postsPerPage, setPostsPerPage ] = useState(6)
    const [ id, setId ] = useState(null);
    const [ data, setData ] = useState({});
    const [ show, setShow ] = useState(false);
    const [ linea, setLinea ] = useState(null);
    const [ lineaSelect, setLineaSelect ] = useState([]);
    const [ subLinea, setSublinea ] = useState(null);
    const [ subLineaSelec, setSubLineaSelect ] = useState([]);
    const [ codigoProd, setCodigoProd ] = useState(null);
    const [ precioProd, setPrecioProd ] = useState(null);
    const [ descProd, setDescProd ] = useState(null);
    const [ stockProd, setStockProd ] = useState(null);
    const [ listProducto, setListProducto ] = useState([]);
    const [ nomProducto, setNomProducto ] = useState(null);
    const [ backProduct, setBackProduct ] = useState(listProducto);
    const [ backProduct2, setBackProduct2 ] = useState(listProducto);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

  const modal = async () => {
    setShow(true);
  }


    useEffect(()=>{
        obtain_linea();
        //obtain_sublinea();
        validar();
    }, []);
    
    const obtain_linea = async () => {
        console.log("Entro linea")
        axios.get(ip+'obtain_linea').then(response=>{
          var res = response.data;
          var linea = [];
          res.map(item=>{
            const data = {
              value:item.codigo,
              label:item.descripcion
            }
            linea.push(data)
            setLinea(linea)
          })
          var filtro = res.filter(e=>e.codigo == props.data.id_linea);
          var categ = []
          filtro.map(item=>{
            const data = {
              value:item.codigo,
              label:item.descripcion
            }
            obtain_sublinea(item.codigo, false)
            categ.push(data)
            setLineaSelect(data)
          })
      })
    }
    
    const obtain_sublinea = async (data, value) => {
        setSubLineaSelect([])
        axios.get(ip+'obtain_sublinea').then(response=>{
          var res = response.data;
          console.log(props.data.id_sublinea)
          var sublinea = []
          var filtro = response.data.filter(e=>e.id_categoria == data);
          var filtro2 = filtro.filter(e=>e.id_subcategoria == props.data.id_sublinea);
          filtro.map(item=>{
            var data = {
              value:item.id_subcategoria,
              label:item.descripcion
            }
            sublinea.push(data)
          })
          setSublinea(sublinea);
          
          filtro2.map(item=>{
            const data = {
              value:item.id_subcategoria,
              label:item.descripcion
            }
            setSubLineaSelect(data)
          })
      })
    }

    //// funciones para recojer dato de categoría y hacer filtro
    const categorie = (value)=>{
      setLineaSelect(value);
      obtain_sublinea(value.value, true)
    }

    const MessageError = async (data) => {
        Swal.fire({
          title: 'Error',
          text: data,
          icon: 'warning',
        })
    }
    
    const MessageSuccess = async (data) => {
        Swal.fire({
          text: data,
          icon: 'success',
        })
    }
    
    const validar = async () =>{
        if(props.id > 0){
            setCodigoProd(props.data.codigo_producto)
            setNomProducto(props.data.nom_producto)
            setDescProd(props.data.descripcion)
            setPrecioProd(props.data.costo_ultimo)
            setStockProd(props.data.stock)
        }
     
    }

    const save_product = async() => {
        var message = ''
        var error = false
        if (codigoProd == null){
          error = true
          message = "Escribe el código del producto "
        }else if(lineaSelect == ''){
          error = true
          message = "Elige la línea del producto "
        }else if(subLineaSelec == ''){
          error = true
          message = "Elige la sublínea del producto "
        }else if(nomProducto == null){
          error = true
          message = "Escribe el nombre del producto "
        }else if(descProd == null){
          error = true
          message = "Escribe la descripción del producto "
        }
        if(error){
            MessageError(message)
        }
        else{
          const data = new FormData()
          data.append('id',props.id)
          data.append('codigo_producto', codigoProd)
          data.append('id_linea', lineaSelect.value)
          data.append('id_sublinea', subLineaSelec.value)
          data.append('nom_producto', nomProducto)
          data.append('descripcion', descProd)
          axios.post(ip+'guard_producto',data).then(response=>{
              if(props.id == null){
                MessageSuccess("Producto creado correctamente");
                props.handleClose();
              }else{
                MessageSuccess("Producto editado correctamente");
                props.handleClose();
            }
          })
        }
      }

      

    return(
        <div>
     
              <div class="container-fluid">
                <div class="row">
                <div class="mb-3">
                  <label for="recipient-name" className="col-form-label">Código del producto:</label>
                  <input type="text" className="form-control" placeholder="Ingrese el código del producto" value={codigoProd} onChange={(event)=>setCodigoProd(event.target.value)}/>
                </div>
                <div class="mb-3">
                  <label for="recipient-name" class="col-form-label">Línea del producto:</label>
                  <Select
                  value={lineaSelect}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={linea}
                  onChange={(e)=>categorie(e)}
                  placeholder = "Seleccionar línea del producto"
                  name="colors"
                  />
                </div>
                <div class="mb-3">
                  <label for="recipient-name" class="col-form-label">Sublínea del producto:</label>
                  <Select
                  value={subLineaSelec}
                  closeMenuOnSelect={true}
                  components={animatedComponents}
                  options={subLinea}
                  onChange={(e)=>setSubLineaSelect(e)}
                  placeholder = "Seleccionar sublínea del producto"
                  name="colors"
                  />
                </div>
                <div class="mb-3">
                  <label for="recipient-name" class="col-form-label">Nombre del producto:</label>
                  <input type="text" class="form-control" placeholder="Ingrese el nombre del producto" value={nomProducto} onChange={(event)=>setNomProducto(event.target.value)}/>
                </div>
                {/*
                <div class="mb-3">
                  <label for="recipient-name" class="col-form-label">Precio del producto:</label>
                  <input type="text" class="form-control" placeholder="Ingrese el precio del producto" value={precioProd} onChange={(event)=>setPrecioProd(event.target.value)}/>
                </div>
                */}
                <div class="mb-3">
                  <label for="recipient-name" class="col-form-label">Descripción del producto:</label>
                  <textarea class="form-control" placeholder="Ingrese la descripción del producto" value={descProd} onChange={(event)=>setDescProd(event.target.value)}/>
                </div>
                {/*
                <div class="mb-3">
                  <label for="recipient-name" class="col-form-label">Stock del producto:</label>
                  <input type="text" class="form-control" placeholder="Ingrese el stock del producto" value={stockProd} onChange={(event)=>setStockProd(event.target.value)}/>
                </div>
                */}
                </div>
              </div>
              <Modal.Footer>
             <Button variant="primary" onClick = {()=>save_product()}>Guardar producto</Button>
             <Button variant="danger" onClick = {()=>props.handleClose()}>Cerrar</Button>
            </Modal.Footer>
      </div>
    );
}

export default ModalRegister;

if (document.getElementById('ModalRegister')) {
  ReactDOM.render(<ModalRegister />, document.getElementById('ModalRegister'));
}