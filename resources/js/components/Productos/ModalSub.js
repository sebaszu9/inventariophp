import React, { useState, useEffect  }  from 'react';
import '../../../css/app.scss';
import Select from 'react-select';
import makeAnimated from 'react-select/animated'
import {ip} from '../ApiRest';
import Swal from 'sweetalert2';
import { Button, Modal } from 'react-bootstrap';

function ModalSub(props) {

    const [ animatedComponents, setAnimatedComponents ] = useState(makeAnimated)
    const [categoria , setCategoria] = useState (null);
    const [categoriaSelect , setCategoriaSelect] = useState([]);
    const [nombreSub , setNombreSub] = useState('');

    useEffect(()=>{
        obtain_linea();
    }, []);

    const MessageError = async (data) => {
        Swal.fire({
          title: 'Error',
          text: data,
          icon: 'warning',
        })
    }
    
    const save_sub = async() => {
        ///Aqui es la funcion para guardar categoría
        var message = ''
        var error = false

        if (categoriaSelect == ''){
          error = true
          message = "Seleccione una categoría "
        }else if (nombreSub == ''){
            error = true
            message = "Escriba el nombre de la subcategoría "
        }
        if(error){
          MessageError(message)
        }
        else{
          const data = new FormData()
          data.append ('id_subcategoria', props.id_subcategoria)
          data.append ('id_categoria', categoriaSelect.value)
          data.append ('descripcion', nombreSub)
          axios.post(ip+'guard_sub',data).then(response=>{
            if(props.idSub == null){
              MessageSuccess("Subcategoria creada correctamente");
              props.handleClose();
            }
          })
        }
        
    }

    const MessageSuccess = async (data) => {
        Swal.fire({
          text: data,
          icon: 'success',
        })
    }

    const obtain_linea = async () => {
        console.log("Entro linea")
        axios.get(ip+'obtain_linea').then(response=>{
          var res = response.data;
          var categoria = [];
          res.map(item=>{
            const data = {
              value:item.codigo,
              label:item.descripcion
            }
            categoria.push(data)
            setCategoria(categoria)
          })
      })
    }
    return(
        <div>
     
              <div class="container-fluid">
                <div class="row">
                    <div class="mb-3">
                    <label for="recipient-name" class="col-form-label">Categoria:</label>
                    <Select
                    value={categoriaSelect}
                    closeMenuOnSelect={true}
                    components={animatedComponents}
                    options={categoria}
                    onChange={(e)=>setCategoriaSelect(e)}
                    placeholder = "Seleccionar categoria"
                    name="colors"
                    />
                    </div>  
                    <div class="mb-3">
                        <label for="recipient-name" className="col-form-label">Nombre de la subcategoría:</label>
                        <input type="text" className="form-control" placeholder="Ingrese el nombre de la subcategoría" value={nombreSub} onChange={(event)=>setNombreSub(event.target.value)}/>
                    </div>
                </div>
              </div>
              <Modal.Footer>
             <Button variant="primary" onClick = {()=>save_sub()}>Guardar subcategoría</Button>
             <Button variant="danger" onClick = {()=>props.handleClose()}>Cerrar</Button>
            </Modal.Footer>
      </div>
    );
}

export default ModalSub;

if (document.getElementById('ModalSub')) {
  ReactDOM.render(<ModalSub />, document.getElementById('ModalSub'));
}