import React, { useState, useEffect  }  from 'react';
import ReactDOM from 'react-dom';
import '../../../css/app.scss';
import Select from 'react-select';
import makeAnimated from 'react-select/animated'
import {ip} from '../ApiRest';
import Swal from 'sweetalert2';
import { Button, Modal } from 'react-bootstrap';
import useful from "../util";
import Pagination from '../pagination/Pagination';
import PaginationButton from '../pagination/Pagination-button';

import Registro from './Modal';
import Categoria from './ModalCat';
import Subcategoria from './ModalSub';

function Productos(props) {

  const [loading, setLoading] = useState(false)

  const [animatedComponents, setAnimatedComponents] = useState(makeAnimated)
  const [currentPage, setCurrentPage] = useState(1)
  const [postsPerPage, setPostsPerPage] = useState(6)
  const [id, setId] = useState(null);
  const [data, setData] = useState({});
  const [show, setShow] = useState(false);
  const [showCat, setShowCat] = useState(false);
  const [showSub, setShowSub] = useState(false);
  const [linea, setLinea] = useState(null);
  const [lineaSelect, setLineaSelect] = useState([]);
  const [subLinea, setSublinea] = useState(null);
  const [subLineaSelec, setSubLineaSelect] = useState([]);
  const [codigoProd, setCodigoProd] = useState(null);
  const [precioProd, setPrecioProd] = useState(null);
  const [descProd, setDescProd] = useState(null);
  const [stockProd, setStockProd] = useState(null);
  const [listProducto, setListProducto ] = useState([]);
  const [backProduct, setBackProduct] = useState(listProducto);
  const [backProduct2, setBackProduct2] = useState(listProducto);

  const [validar, setValidar] = useState(false)

  const handleClose = () =>{
    setShow(false);
    setShowCat(false);
    setShowSub(false);
    setValidar(false)
    mostrarProducto()
    setId (null);
  }

  const updateCurrentPage = async (number) => {
    await setListProducto([])
    await setCurrentPage(number)
    const dataNew = Pagination.paginate(backProduct,number,postsPerPage)
    await setListProducto(dataNew)
  }

  const handleShow = () => setShow(true);

  const modal = async () => {
    setShow(true);
  }

  const modalCate = async () => {
    setShowCat(true);
  }

  const modalSub = async () => {
    setShowSub(true);
  }

  useEffect(()=>{
    mostrarProducto();
    //validar();
}, []);

  const mostrarProducto = async() =>{
    axios.get(ip+'obtain_Product').then(response=>{
      console.log(response);
      var res = response.data;
      setBackProduct(response.data)
      setBackProduct2(response.data)
      const dataNew = Pagination.paginate(response.data,currentPage,postsPerPage)
      setListProducto(dataNew)
    })
  }

  const dataUpdate = async (data, valor) => {
    setLoading(true)
    console.log("Aqui dataUpdate")
    console.log(data);

    var codigo_producto = data.codigo_producto
    var id_linea = data.id_linea
    var id_sublinea = data.id_sublinea
    var nom_producto = data.nom_producto
    var descripcion = data.descripcion
    var costo_ultimo = data.costo_ultimo
    var stock = data.stock

    const dataForAll = {
      codigo_producto,
      id_linea,
      id_sublinea,
      nom_producto,
      descripcion,
      costo_ultimo,
      stock,
    }
    setValidar(valor)
    await setId(data.id)
    await setData(dataForAll)
    await setShow(true);
    console.log("Aqui data forAll")
    console.log(dataForAll);
    setLoading(false)
    setData({})
  }

  const deleted = (value) =>{
    //var message = '¿Quieres eliminar esta producto ${value.codigo}?'
    Swal.fire({
      title: `¿Quieres eliminar este producto ${value.nom_producto}?`,
      showDenyButton: true,
      confirmButtonText: `Sí`,
      denyButtonText: `No`,

    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const data = new FormData()
        data.append('id', value.id)
        axios.post(ip+'deleted_product',data).then(response=>{

          Swal.fire('Producto eliminado correctamente!', '', 'success')
          mostrarProducto();
        })

      } else if (result.isDenied) {
        Swal.fire('Acción cancelada', '', 'info')
      }
    })
  }

  return (
  <section className = "container-app">
    <div className="container mt-5"  >
        <div className="btn container col-1 "><a href ={ip+"inventario"}>
          <h4 className="form-section d-flex align-items-center"><i className="material-icons"> arrow_back_ios</i>Regresar</h4></a>
        </div>
        <div className="text-center">
        <h4><strong>Módulo de productos</strong></h4>
        </div>
        <div className="row mb-3">
        <div className="col-md-12 row mb-3 p-0 mt-4">
          <div className="row m-t-30">
            <div className="mt-4 mb-3">
            <button onClick = {()=>modal()} type="button" class="btn btn-color"> <span className="material-icons icon-btn">add</span>Agregar producto</button>

            <button onClick = {()=>modalCate()} type="button" class="btn btn-color" style={{marginLeft: '15px'}}> <span className="material-icons icon-btn">add</span>Agregar categoría</button>

            <button onClick = {()=>modalSub()} type="button" class="btn btn-color" style={{marginLeft: '15px'}}> <span className="material-icons icon-btn">add</span>Agregar subcategoria</button>
            </div>
            <div className="col-md-12">
              <table className="table table-hover" style={{marginBottom: '40px', paddingRight: '1px'}}>
                <thead className="table-dark">
                  <tr>
                    <th>Id</th>
                    <th>Código del producto</th>
                    <th>Nombre</th>
                    <th>Linea</th>
                    <th>Sublinea</th>
                    <th>Descripcion</th>
                    <th style={{width: '100px'}}>Precio</th>
                    <th>Stock</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                  </tr>
                </thead>
                <tbody>
                {
                  listProducto.map((item)=>{
                    return(
                      <tr>
                        <th scope="row">{item.id}</th>
                        <td>{item.codigo_producto}</td>
                        <td>{item.nom_producto}</td>
                        <td><strong>{item.tipo_linea.descripcion}</strong></td>
                        <td>{item.tipo_sublinea.descripcion}</td>
                        <td>{item.descripcion}</td>
                        <td>$ {useful.convertMoney(item.costo_ultimo)}</td>
                        <td>{item.stock}</td>
                        <td>
                          <div>
                          <button onClick = {()=>dataUpdate(item, true)} className="btn "><i class = "material-icons">edit</i></button>
                          </div>
                        </td>
                        <td>
                          <div>
                          <button onClick = {()=>deleted(item)} className="btn "><i class = "material-icons">delete</i></button>
                          </div>
                        </td>
                      </tr>
                    )
                  })
                }
                </tbody>
              </table>
              <div className="d-flex col-md-12 col-12 justify-content-end">
                <PaginationButton currentPage={currentPage} postsPerPage={postsPerPage} totalData={backProduct.length} updateCurrentPage={(number)=>updateCurrentPage(number)}/>
              </div>
              </div>
            </div>
            <Modal show={show} onHide={handleClose}>
           <Modal.Header closeButton>
             <Modal.Title>{id > 0 ? 'Editar productos':'Agregar productos'}</Modal.Title>
           </Modal.Header>
             <Modal.Body>
               <Registro handleClose={()=>handleClose()} data={data} nom={validar} id = {id}/>
             </Modal.Body>
         </Modal>

         <Modal show={showCat} onHide={handleClose}>
           <Modal.Header closeButton>
             <Modal.Title>Agregar categorías</Modal.Title>
           </Modal.Header>
             <Modal.Body>
               <Categoria handleClose={()=>handleClose()}/>
             </Modal.Body>
         </Modal>

         <Modal show={showSub} onHide={handleClose}>
           <Modal.Header closeButton>
             <Modal.Title>Agregar subcategoria</Modal.Title>
           </Modal.Header>
             <Modal.Body>
               <Subcategoria handleClose={()=>handleClose()}/>
             </Modal.Body>
         </Modal>
          </div>
        </div>
    </div>
  </section>
    );
  }
  export default Productos;

  if (document.getElementById('Productos')) {
    ReactDOM.render(<Productos />, document.getElementById('Productos'));
  }