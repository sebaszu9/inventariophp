import React, { useState, useEffect  }  from 'react';
import '../../../css/app.scss';
import Select from 'react-select';
import makeAnimated from 'react-select/animated'
import {ip} from '../ApiRest';
import Swal from 'sweetalert2';
import { Button, Modal } from 'react-bootstrap';

function ModalCat(props) {

    const [ nombreCat, setNombreCat ] = useState('');

    useEffect(()=>{

    }, []);
    

    const MessageError = async (data) => {
        Swal.fire({
          title: 'Error',
          text: data,
          icon: 'warning',
        })
    }
    
    const MessageSuccess = async (data) => {
        Swal.fire({
          text: data,
          icon: 'success',
        })
    }

    const save_cat = async() => {
        ///Aqui es la funcion para guardar categoría
        var message = ''
        var error = false

        if (nombreCat == ''){
          error = true
          message = "Escribe el nombre de la categoría "
        }
        if(error){
          MessageError(message)
        }
        else{
          const data = new FormData()
          data.append ('codigo', props.codigo)
          data.append ('descripcion', nombreCat)
          axios.post(ip+'guard_categoria',data).then(response=>{
            if(props.codigo == null){
              MessageSuccess("Categoria creada correctamente");
              props.handleClose();
            }
          })
        }
        
    }

    return(
        <div>
     
              <div class="container-fluid">
                <div class="row">
                  <div class="mb-3">
                    <label for="recipient-name" className="col-form-label">Nombre de la categoría:</label>
                    <input type="text" className="form-control" placeholder="Ingrese el nombre de la categoría" value={nombreCat} onChange={(event)=>setNombreCat(event.target.value)}/>
                  </div>
                </div>
              </div>
              <Modal.Footer>
             <Button variant="primary" onClick = {()=>save_cat()}>Guardar categoría</Button>
             <Button variant="danger" onClick = {()=>props.handleClose()}>Cerrar</Button>
            </Modal.Footer>
      </div>
    );
}

export default ModalCat;

if (document.getElementById('ModalCat')) {
  ReactDOM.render(<ModalCat />, document.getElementById('ModalCat'));
}