const pagination = {}

pagination.paginate = (data,currentPage,postsPerPage) => {
  // console.log(data);

  const indexLastPost = currentPage * postsPerPage
  const indexFirstPost = indexLastPost - postsPerPage

  const objeto = data.slice(indexFirstPost, indexLastPost)

  return objeto
}

export default pagination