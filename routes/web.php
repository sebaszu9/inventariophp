<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/inventario', [App\Http\Controllers\ProductoController::class, 'inventario']);

///Productos
Route::get('/productos', [App\Http\Controllers\ProductoController::class, 'productos']);
Route::get('/obtain_linea', [App\Http\Controllers\ProductoController::class, 'obtain_linea']);
Route::get('/obtain_sublinea', [App\Http\Controllers\ProductoController::class, 'obtain_sublinea']);
Route::post('/guard_producto', [App\Http\Controllers\ProductoController::class, 'guard_producto']);
Route::get('/obtain_Product', [App\Http\Controllers\ProductoController::class, 'obtain_Product']);
Route::post('/deleted_product',[App\Http\Controllers\ProductoController::class, 'deleted_product']);
Route::post('/guard_categoria',[App\Http\Controllers\ProductoController::class, 'guard_categoria']);
Route::post('/guard_sub',[App\Http\Controllers\ProductoController::class, 'guard_sub']);
///Movimiento
Route::get('/movimientos', [App\Http\Controllers\MovimientoController::class, 'movimientos']);
Route::post('/guard_movimiento', [App\Http\Controllers\MovimientoController::class, 'guard_movimiento']);
Route::get('/obtain_Movimiento', [App\Http\Controllers\MovimientoController::class, 'obtain_Movimiento']);
Route::get('/reportes', [App\Http\Controllers\MovimientoController::class, 'download']);

///Login
Route::get('/encrypted/pass', [App\Http\Controllers\LoginController::class, 'encrypted']);
Route::post('/login_administrador', [App\Http\Controllers\LoginController::class, 'login_admon']);


